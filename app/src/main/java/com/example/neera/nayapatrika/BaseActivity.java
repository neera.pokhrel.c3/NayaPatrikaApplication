package com.example.neera.nayapatrika;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neera.nayapatrika.adapter.CustomListView;
import com.example.neera.nayapatrika.database.ReportData;
import com.example.neera.nayapatrika.model.DrawerMenuItems;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by neera on 2/24/16.
 */
public class BaseActivity extends AppCompatActivity {
    public DrawerLayout drawerLayout;
    protected Toolbar toolbar;
    private ListView lv_listView;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private CustomListView listViewAdapter;
    private ArrayList<DrawerMenuItems> drawerMenuItems;
    private JSONActivity jsonActivity;
    private final String URL = "http://192.168.100.5/BackendNayaPatrika/Api/addReportData";
    private Realm realm;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.nav_lay);


        toolbar = (Toolbar) findViewById(R.id.toolbar);

        drawerMenuItems = new ArrayList<>();


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

//            toolbar.setNavigationIcon(R.drawable.circle);
        }


        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowTitleEnabled(true);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerLayout.setDrawerListener(mActionBarDrawerToggle);


        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();


            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
            }
        };

        drawerLayout.setDrawerListener(mActionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });


        addDrawerMenu();

    }


    protected void setActionBarIcon(int iconRes) {
        toolbar.setNavigationIcon(iconRes);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START);
            else
                drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // drawerToggle.syncState();
    }

    protected void appendView(int v) {
        ViewGroup vi = (ViewGroup) findViewById(R.id.root);
        if (vi != null)
            LayoutInflater.from(this).inflate(v, vi);
    }

    protected void appendView(View v) {
        ViewGroup vi = (ViewGroup) findViewById(R.id.root);
        vi.addView(v);
    }


    private void addDrawerMenu() {
        lv_listView = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.headerlayout, null, false);

        lv_listView.addHeaderView(listHeaderView);

        final DrawerMenuItems list_data[] = new DrawerMenuItems[]
                {
                        new DrawerMenuItems("कभरस्टोरी", R.drawable.cover),
                        new DrawerMenuItems("जिल्ला", R.drawable.district),
                        new DrawerMenuItems("राष्ट्रिय", R.drawable.rastriya),
                        new DrawerMenuItems("दृष्टिकोण", R.drawable.thinking),
                        new DrawerMenuItems("अर्थ", R.drawable.arthaa),
                        new DrawerMenuItems("खेलकुद", R.drawable.sports),
                        new DrawerMenuItems("विश्व खबर", R.drawable.world),
                        new DrawerMenuItems("आगन्तुक ", R.drawable.aagantuk),
                        new DrawerMenuItems("सप्तरंग", R.drawable.sapta),
                        new DrawerMenuItems("सम्पादकीय ", R.drawable.pen),
                        new DrawerMenuItems("स्वास्थ्य", R.drawable.health),


                };

        CustomListView listViewAdapter = new CustomListView(this,
                R.layout.drawer_list_item, list_data);

        lv_listView.setAdapter(listViewAdapter);

        lv_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                switch (position) {
                    case 1:

//                        addToDatabaseOrSend("कभरस्टोरी");
                        addToDatabaseOrSend("कभरस्टोरी");
                        Intent intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "कभरस्टोरी");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/coverstory/feed");
                        startActivity(intent);
//                       jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/coverstory/feed","coverstory");
                        break;
                    case 2:

                        addToDatabaseOrSend("जिल्ला");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "जिल्ला");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/district/coverstory/feed");
                        startActivity(intent);
//                       jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/district/feed","district");
                        break;
                    case 3:

                        addToDatabaseOrSend("राष्ट्रिय");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "राष्ट्रिय");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/national/coverstory/feed");
                        startActivity(intent);
//                       jsonActivity=  new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/national/feed","national");
                        break;
                    case 4:

                        addToDatabaseOrSend("दृष्टिकोण");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "दृष्टिकोण");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/opinion/coverstory/feed");
                        startActivity(intent);
//                       jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/opinion/feed","opinion");
                        break;
                    case 5:

                        addToDatabaseOrSend("अर्थ");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "अर्थ");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/business/feed");
                        startActivity(intent);
//                        jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/business/feed","business");
                        break;
                    case 6:

                        addToDatabaseOrSend("खेलकुद");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "खेलकुद");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/sports/feed");
                        startActivity(intent);
//                       jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/sports/feed","sports");
                        break;
                    case 7:

                        addToDatabaseOrSend("विश्व खबर");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "विश्व खबर");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/world/feed");
                        startActivity(intent);
//                        jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/world/feed","world");
                        break;
                    case 8:

                        addToDatabaseOrSend("आगन्तुक");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "आगन्तुक");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/aagantuk/feed");
                        startActivity(intent);
//                       jsonActivity= new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/aagantuk/feed","aagantuk");
                        break;
                    case 9:

                        addToDatabaseOrSend("सप्तरंग");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "सप्तरंग");
                        intent.putExtra("url","https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/saptaranga/feed");
                        startActivity(intent);
//                      jsonActivity=  new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/saptaranga/feed","saptaranga");
                        break;
                    case 10:

                        addToDatabaseOrSend("सम्पादकीय");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "सम्पादकीय");
                        intent.putExtra("url", "https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/editorial/feed");
                        startActivity(intent);

//                     jsonActivity=   new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/editorial/feed","editorial");
                        break;
                    case 11:

                        addToDatabaseOrSend("स्वास्थ्य र जीवनशैली");
                        intent = new Intent(BaseActivity.this,
                                JSONActivity.class);
                        intent.putExtra("category", "स्वास्थ्य र जीवनशैली");
                        intent.putExtra("url", "https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/%E0%A4%B8%E0%A5%8D%E0%A4%B5%E0%A4%BE%E0%A4%B8%E0%A5%8D%E0%A4%A5%E0%A5%8D%E0%A4%AF-%E0%A4%B0-%E0%A4%9C%E0%A5%80%E0%A4%B5%E0%A4%A8%E0%A4%B6%E0%A5%88%E0%A4%B2%E0%A5%80/feed");
                        startActivity(intent);
//                      jsonActivity=  new JSONActivity("https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=http://www.enayapatrika.com/category/%E0%A4%B8%E0%A5%8D%E0%A4%B5%E0%A4%BE%E0%A4%B8%E0%A5%8D%E0%A4%A5%E0%A5%8D%E0%A4%AF-%E0%A4%B0-%E0%A4%9C%E0%A5%80%E0%A4%B5%E0%A4%A8%E0%A4%B6%E0%A5%88%E0%A4%B2%E0%A5%80/feed","health");
                        break;
//
                }
//                Intent i = new Intent(BaseActivity.this, jsonActivity.getClass());
//                startActivity(i);

            }
        });


    }


    private void addToDatabaseOrSend(final String category) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Date date = new Date();
        final String currentDateAndTime = dateFormat.format(date); //2016/04/06 15:59:48

        if (isNetworkAvailable()) {



            RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest req = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("ADD REPORT", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.i("ADD REPORT", error.toString());
                }
            }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("category", category + "");
                    params.put("date", currentDateAndTime + "");
                    return params;
                }
            };

            mRequestQueue.add(req);


        } else {


            realm = Realm.getInstance(getApplicationContext());
            realm.beginTransaction();
            ReportData item = realm.createObject(ReportData.class);
            item.setId(UUID.randomUUID().toString());
            item.setCategoty(category);
            item.setDate(currentDateAndTime);
            realm.commitTransaction();

        }

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
