package com.example.neera.nayapatrika.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neera.nayapatrika.R;
import com.example.neera.nayapatrika.model.DrawerMenuItems;

/**
 * Created by neera on 2/24/16.
 */
public class CustomListView extends ArrayAdapter<DrawerMenuItems> {
    Context context;
    int layoutResourceId;
    DrawerMenuItems newsCat[] = null; ;
    public CustomListView(Context context,int layoutResourceId, DrawerMenuItems newsCat[]  ) {
        super(context, layoutResourceId, newsCat);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.newsCat = newsCat;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ListHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.drawer_item_image);
            holder.txtTitle = (TextView)row.findViewById(R.id.drawer_item_text);

            row.setTag(holder);
        }
        else
        {
            holder = (ListHolder) row.getTag();
        }

        DrawerMenuItems menuItems = newsCat[position];
        holder.txtTitle.setText(menuItems.getMenuName());
        holder.imgIcon.setImageResource(menuItems.getImageId());

        return row;
    }

    static class ListHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
