package com.example.neera.nayapatrika;

import android.content.Context;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.neera.nayapatrika.database.RssItems;
import com.example.neera.nayapatrika.model.JustifiedTextView;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import io.realm.Realm;
import io.realm.RealmResults;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;


/**
 * Created by neera on 3/20/16.
 */
public class DetailActivity extends AppCompatActivity {

    Realm realm;
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        Twitter twitter = new TwitterFactory().getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.backarrow);
        }


        ScrollView sv = (ScrollView) findViewById(R.id.sv);
        sv.setVerticalFadingEdgeEnabled(true);

        String itemId = null;

        if (getIntent().hasExtra("itemId")){
            itemId = getIntent().getStringExtra("itemId");
        }

        realm = Realm.getInstance(getApplicationContext());
        final RealmResults<RssItems> results =
                realm.where(RssItems.class).equalTo("id",itemId)
                        .findAll();
        if (results.size()!= 0) {
            // Initialize the views
            TextView title = (TextView) findViewById(R.id.title);
            TextView top = (TextView) findViewById(R.id.topPanel);
            JustifiedTextView tv_content = (JustifiedTextView) findViewById(R.id.TV_detail);
            ImageView imageView = (ImageView) findViewById(R.id.IV_detail);
            if (!results.get(0).getImage().isEmpty()) {
                imageView.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,300);
                layoutParams.addRule(RelativeLayout.BELOW,R.id.IV_detail);
                tv_content.setLayoutParams(layoutParams);
                Picasso.with(getApplicationContext()).load(results.get(0).getImage()).into(imageView);
            } else {
                imageView.setVisibility(View.GONE);
            }
            top.setText((results.get(0).getCategory()));
            title.setText(results.get(0).getTitle());
            tv_content.setText(results.get(0).getContentencoded());
            tv_content.setAlignment(Paint.Align.LEFT);
            tv_content.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);


            //title.setText(Html.fromHtml(feed.getItem(pos).getTitle()));
//        desc.loadDataWithBaseURL("http://www.enayapatrika.com/category/coverstory/feed", feed
//                .getItem(pos).getDescription(), "text/html", "UTF-8", null);
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(results.get(0).getLink()))
                    .build();
            Button bt_tweet = (Button) findViewById(R.id.BT_tweetshare);
            ShareButton shareButton = (ShareButton) findViewById(R.id.IB_fbshare);


                shareButton.setShareContent(content);

                bt_tweet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isNetworkAvailable()) {
                            if (!results.get(0).getImage().isEmpty()) {
                                final TweetComposer.Builder builder
                                        = new TweetComposer.Builder(DetailActivity.this)
                                        .text(results.get(0).getTitle() + "\n " + results.get(0).getLink())
                                        .image(Uri.parse(results.get(0).getImage()));
                                builder.show();
                            } else {
                                final TweetComposer.Builder builder
                                        = new TweetComposer.Builder(DetailActivity.this)
                                        .text(results.get(0).getTitle() + "/n" + results.get(0).getDescription());
                                builder.show();

                            }

                        }

                        else{
                            Toast.makeText(getApplicationContext(), "Please connect to the Internet!", Toast.LENGTH_SHORT).show();

                        }
                    }


                });




        }
            //create the send intent

        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static String replaceAllChar(String s, String f, String r){
        String temp = s.replace(f ,r);
        return temp;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    }


