package com.example.neera.nayapatrika.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neera.nayapatrika.R;
import com.example.neera.nayapatrika.database.RssItems;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created by neera on 3/20/16.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private Activity mActivity;
    private List<RssItems> rssItems;
    private View.OnClickListener mOnClickListener;

    public NewsListAdapter(Activity mActivity, List<RssItems> rssItems, View.OnClickListener mOnClickListener) {
        this.mActivity = mActivity;
        this.rssItems = rssItems;
        this.mOnClickListener = mOnClickListener;
    }

    @Override
    public NewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rss_list_item, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new ViewHolder(view);

    }


    @Override
    public void onBindViewHolder(NewsListAdapter.ViewHolder holder, int position) {

        holder.postname.setText(rssItems.get(position).getTitle());
        try {
            Picasso.with(mActivity).load(rssItems.get(position).getImage()).resize(70, 60).into(holder.iv);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        String dateString = sdf.format(rssItems.get(position).getDate());
        holder.date.setText(dateString);
    }

    @Override
    public int getItemCount() {
        return rssItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView postname;
        TextView date;
        ImageView iv;

        public ViewHolder(View itemView) {
            super(itemView);


            postname = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            iv = (ImageView) itemView.findViewById(R.id.thumb);


        }
    }
}
