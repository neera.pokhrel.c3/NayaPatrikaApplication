package com.example.neera.nayapatrika.service;

/**
 * Created by neera on 4/3/16.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.neera.nayapatrika.MainActivity;
import com.example.neera.nayapatrika.R;
import com.example.neera.nayapatrika.database.RssItems;
import com.google.android.gms.gcm.GcmListenerService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmQuery;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    SharedPreferences mSharedPreferences;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        String date = data.getString("date");
        DateFormat df5 = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Date pubDate =  new Date();
        try {
            pubDate = df5.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Realm realm = Realm.getInstance(getApplicationContext());
        RealmQuery<RssItems> query = realm.where(RssItems.class)
                .equalTo("date", pubDate);
        if (query.count() == 0) {
            realm = Realm.getInstance(getApplicationContext());
            realm.beginTransaction();
            RssItems item = realm.createObject(RssItems.class);
            item.setId(UUID.randomUUID().toString());
            item.setTitle(data.getString("title"));
            item.setDescription(data.getString("contentencoded"));
            item.setContentencoded(data.getString("contentencoded"));
            item.setAuthor(data.getString("author"));
            item.setDate(pubDate);
            item.setLink(data.getString("image"));
            item.setImage(data.getString("image"));
            item.setCategory(data.getString("category"));
            realm.commitTransaction();
        }


        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        String title = data.getString("title");
        String content = data.getString("contentencoded");


        mSharedPreferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
       boolean sendNotification = mSharedPreferences.getBoolean("notify",true);


        // notification settings checking
        if (sendNotification)
            sendNotification(title, content);

        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.alertlogo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
