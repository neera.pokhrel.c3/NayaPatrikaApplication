package com.example.neera.nayapatrika.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neera.nayapatrika.R;
import com.example.neera.nayapatrika.database.RssItems;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by neera on 9/22/15.
 */
public class MyListAdapter extends ArrayAdapter<RssItems> {

    private LayoutInflater inflater;

    public MyListAdapter(Context context, List<RssItems> objects) {
        super(context, R.layout.rss_list_item, objects);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        {
            View view = inflater.inflate(R.layout.rss_list_item, parent, false);
            TextView postname = (TextView) view.findViewById(R.id.title);
            TextView date = (TextView) view.findViewById(R.id.date);
            ImageView iv = (ImageView)  view.findViewById(R.id.thumb);
            RssItems item = getItem(position);
            postname.setText(item.getTitle());
            if(!item.getImage().isEmpty()){
                Picasso.with(getContext()).load(item.getImage()).into(iv);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
            String dateString = sdf.format(item.getDate());
            date.setText(dateString);
            return view;

        }
    }


}
