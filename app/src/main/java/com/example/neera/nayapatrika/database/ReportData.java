package com.example.neera.nayapatrika.database;

import io.realm.RealmObject;

/**
 * Created by neera on 3/17/16.
 */
public class ReportData extends RealmObject {

    private  String id ;
    private String categoty;
    private String date;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoty() {
        return categoty;
    }

    public void setCategoty(String categoty) {
        this.categoty = categoty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
