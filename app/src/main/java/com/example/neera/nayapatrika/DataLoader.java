package com.example.neera.nayapatrika;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.neera.nayapatrika.database.RssItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by neera on 3/20/16.
 */
public class DataLoader {

    //private  ArrayList<RssItems> items = new ArrayList<>();

    private  Realm realm;
    private static final String TAG = "MyCategory";
    private   RequestQueue requestQueue;
    private ArrayList<RssItems> rssItems;
    private String RssUrl = null;
    private DataLoadedListener mLoadedListener;

    public DataLoader(Activity mActivity, String rssUrl, final DataLoadedListener mLoadedListener ) {
        this.RssUrl = rssUrl;
        this.mLoadedListener= mLoadedListener;

        loadData(mActivity);
    }

    private void loadData(final Activity mActivity){

        requestQueue = Volley.newRequestQueue(mActivity);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, this.RssUrl, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jObjectResult = response.getJSONObject("responseData");
                    JSONObject jObjectResult1 = jObjectResult.getJSONObject("feed");

                    JSONArray ja = jObjectResult1.getJSONArray("entries");

                    for (int i = 0; i < ja.length(); i++) {

                        JSONObject jsonObject = ja.getJSONObject(i);

                        // int id = Integer.parseInt(jsonObject.optString("id").toString());
                        String title = jsonObject.getString("title");
                        //items.setTitle(title);
                        String description = jsonObject.getString("contentSnippet");
                        //items.setDescription(description);
                        String cat = jsonObject.getString("categories");
                        String[] cats = cat.split("\"");
                        String [] cats2 = cats[1].split("\"");
                        String category = cats2[0];
                        Log.d(TAG, "Ca: " + category);
                        String content = jsonObject.getString("content");
                        //items.setContentencoded(content);
                        //JSONObject jsonObject11 = response.getJSONObject("content");
                        String publishedDate = jsonObject.getString("publishedDate");

                        String[] parts = publishedDate.split(",");
                        String [] parts2 = parts[1].split("-");
                        String dateee = parts2[0];
                        DateFormat df5 = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                        Date pubDate = new Date();
                        try {
                             pubDate = df5.parse(dateee);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        // Timestamp ts = Timestamp.valueOf(publishedDate);
                        // Timestamp timestamp = new Timestamp(jsonObject.getLong(date));
                        //items.setDate(date);
                        String author = jsonObject.getString("author");
                        String link = jsonObject.getString("link");
                        // String image = jsonObject11.getString("\\u003e\\u003cimg src\\u003d\\");
                        String image = extractImage(content);
                        String mainContent = android.text.Html.fromHtml(content).toString();

                        realm = Realm.getInstance(mActivity.getApplicationContext());
                        RealmQuery<RssItems> query = realm.where(RssItems.class)
                                .equalTo("date", pubDate);
                        if (query.count() == 0) {
                            realm = Realm.getInstance(mActivity.getApplicationContext());
                            realm.beginTransaction();
                            RssItems item = realm.createObject(RssItems.class);
                            item.setId(UUID.randomUUID().toString());
                            item.setTitle(title);
                            item.setDescription(description);
                            item.setContentencoded(mainContent);
                            item.setAuthor(author);
                            item.setDate(pubDate);
                            item.setLink(link);
                            item.setImage(image);
                            item.setCategory(category);
                            realm.commitTransaction();
                        }
                        //data += "News No " + (i + 1) + " \n News title= " + image + " \n date= " + mainContent + " \n\n\n\n ";
                    }

                    if (mLoadedListener != null) {
                        mLoadedListener.onSuccess();
                    }
                     else {mLoadedListener.onError();}
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("Volley", "Error");

                    }
                }
        );
        requestQueue.add(jor);
    }




    public String extractImage(String stringToSearch) {

        final String regex = "(?<=<img src=\")[^\"]*";
        final Pattern p = Pattern.compile(regex);
        final Matcher m = p.matcher(stringToSearch);
        while (m.find()) {
            return m.group();
        }

        return "0";

    }


    public  interface   DataLoadedListener{
        public void onSuccess();
        public void onError();
    }

}
