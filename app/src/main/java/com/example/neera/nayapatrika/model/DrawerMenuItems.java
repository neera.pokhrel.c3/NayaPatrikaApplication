package com.example.neera.nayapatrika.model;

/**
 * Created by neera on 2/24/16.
 */
public class DrawerMenuItems {

    private String menuName;
    private int imageId;

    public DrawerMenuItems(String menuName, int imageId) {
        this.menuName = menuName;
        this.imageId = imageId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
