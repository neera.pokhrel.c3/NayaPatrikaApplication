package com.example.neera.nayapatrika;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.neera.nayapatrika.adapter.MyListAdapter;
import com.example.neera.nayapatrika.database.RssItems;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * Created by neera on 3/18/16.
 */
public class JSONActivity extends AppCompatActivity {

    TextView output;
    Realm realm;
    String loginURL = "";
    String data = "";
    String category = "";
    String url = "";
    ArrayList<RssItems> items = new ArrayList<>();
    Toolbar toolbar;
    WaveSwipeRefreshLayout swipeRefreshLayout;

    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_list_feed);
        swipeRefreshLayout = (WaveSwipeRefreshLayout) findViewById(R.id.main_swipe);
        TextView tv_label = (TextView) findViewById(R.id.confessid);
        if (savedInstanceState != null) {
            Log.d("STATE", savedInstanceState.toString());
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.backarrow);
        }

        if (getIntent().hasExtra("category")) {
            String category = getIntent().getStringExtra("category");
            String feedUrl = getIntent().getStringExtra("url");
            this.category = category;
            this.url = feedUrl;
        }
        tv_label.setText(category);

        // while swipe refresh is called the url is loaded once again
        swipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isNetworkAvailable()) {

                    new DataLoader(JSONActivity.this, url, new DataLoader.DataLoadedListener() {
                        @Override
                        public void onSuccess() {

                            swipeRefreshLayout.setRefreshing(false);
                            loadData();
                        }

                        @Override
                        public void onError() {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "Please connect to the Internet!", Toast.LENGTH_SHORT).show();

                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });


        loadData(); // load data is called first before the swipe refresh layout

    }


    public void loadData() {

        // requestQueue = Volley.newRequestQueue(this);
        //  output = (TextView) findViewById(R.id.jsonData);

        items.clear();

        realm = Realm.getInstance(getApplicationContext());
        final RealmResults<RssItems> results =
                realm.where(RssItems.class).contains("category", category)
                        .findAllSorted("date", RealmResults.SORT_ORDER_DESCENDING);
        for (int i = 0; i < results.size(); i++) {
            items.add(results.get(i));
        }

        ListView lv_mylist = (ListView) findViewById(R.id.listView);

        MyListAdapter myListAdapter = new MyListAdapter(getApplicationContext(), items);
        lv_mylist.setAdapter(myListAdapter);

        lv_mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(JSONActivity.this, DetailActivity.class);
                String itemId = results.get(position).getId();
                intent.putExtra("itemId", itemId);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String extractImage(String stringToSearch) {

        final String regex = "(?<=<img src=\")[^\"]*";
        final Pattern p = Pattern.compile(regex);
        final Matcher m = p.matcher(stringToSearch);
        while (m.find()) {
            return m.group();
        }

        return "0";

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}