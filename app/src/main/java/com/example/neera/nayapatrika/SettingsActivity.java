package com.example.neera.nayapatrika;

/**
 * Created by neera on 4/2/16.
 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.lb.material_preferences_library.PreferenceActivity;
import com.lb.material_preferences_library.custom_preferences.ListPreference;
import com.lb.material_preferences_library.custom_preferences.SwitchPreference;

public class SettingsActivity extends PreferenceActivity {

    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        final String themePrefKey = getString(R.string.pref_theme), defaultTheme = getResources().getString(R.string.pref_theme_default);
        final String theme = PreferenceManager.getDefaultSharedPreferences(this).getString(themePrefKey, defaultTheme);
        switch (theme) {
            case "dark":
                setTheme(R.style.AppTheme_Dark);
                break;
            case "light":
                setTheme(R.style.AppTheme_Light);
                break;
        }
        super.onCreate(savedInstanceState);
        ListPreference themeListPreference = (ListPreference) findPreference(getString(R.string.pref_theme));
        themeListPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                restartActivity(SettingsActivity.this);
                return true;
            }
        });

        SwitchPreference notificaitonCheck = (SwitchPreference) findPreference(getString(R.string.pref_notification_check));

        
        notificaitonCheck.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (boolean) newValue;
                mSharedPreferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
                mSharedPreferences.edit().putBoolean("notify", checked).apply();
                return true;
            }
        });

        handleActionBarMenu();

        Preference sendFeedback = (Preference) findPreference(getString(R.string.pref_send_feedback));

        sendFeedback.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Intent intentEmail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "letter@nayapatrika.com", null));
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Hi NayaPatrika!");
                intentEmail.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intentEmail, "Select Email client :"));

                return true;
            }
        });


    }

    private void handleActionBarMenu() {
        getToolbar().setNavigationIcon(R.drawable.backarrow);
        getToolbar().inflateMenu(R.menu.pref_menu);
//        getToolbar().inflateMenu(R.menu.activity_main);
        getToolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });

    }


    @Override
    protected int getPreferencesXmlId() {
        return R.xml.preferences;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void restartActivity(final Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            activity.recreate();
        else {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    activity.overridePendingTransition(0, 0);
                    activity.startActivity(activity.getIntent());
                }
            });
            activity.finish();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aboutUs:
                Intent intent = new Intent(SettingsActivity.this, AboutUs.class);
                        startActivity(intent);
                break;


            default:
                break;
        }

        return true;
    }


//    Intent intentEmail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//            "mailto", "info@riverbay.com.np", null));
//    intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Hello Raftfun!");
//    intentEmail.putExtra(Intent.EXTRA_TEXT, "I want some help!");
//    startActivity(Intent.createChooser(intentEmail, "Choose an Email client :"));


}
