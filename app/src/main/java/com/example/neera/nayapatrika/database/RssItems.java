package com.example.neera.nayapatrika.database;


import java.util.Date;

import io.realm.RealmObject;



/**
 * Created by neera on 3/17/16.
 */
public class RssItems extends RealmObject {

    private String id;
    private String title;
    private String description;
    private String contentencoded;
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    private Date date;
    private String image;
    private String link;
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContentencoded() {
        return contentencoded;
    }

    public void setContentencoded(String contentencoded) {
        this.contentencoded = contentencoded;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
