package com.example.neera.nayapatrika;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neera.nayapatrika.adapter.ItemDevider;
import com.example.neera.nayapatrika.adapter.NewsListAdapter;
import com.example.neera.nayapatrika.database.ReportData;
import com.example.neera.nayapatrika.database.RssItems;
import com.example.neera.nayapatrika.service.RegistrationIntentService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;


public class MainActivity extends BaseActivity {

    private final String URL = "http://192.168.100.5/BackendNayaPatrika/Api/addReportData";

    private static final String LOG_TAG = "logMsg";
    //ViewPagerAdapter myViewPagerAdap;
    String[] newsArray;
    Integer[] imgid;
    ImageButton ib_settings;
    ImageButton videos;
    WaveSwipeRefreshLayout swipeRefreshLayout;
    ArrayList<RssItems> items1 = new ArrayList<>();


    NewsListAdapter newsListAdapter;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appendView(R.layout.activity_main);
        ib_settings = (ImageButton) findViewById(R.id.IB_settings);
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        swipeRefreshLayout = (WaveSwipeRefreshLayout) findViewById(R.id.main_swipe);


        //  show news first and call api is there is no news at the beginning
        if (loadDataFromDatabase()) {
            showData();
        }


        // after news is loaded add data to server for generating report
        addDataForReportGeneration();


        swipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isNetworkAvailable()) {
                    showData();
                } else {
                    Toast.makeText(getApplicationContext(), "Please connect to the Internet!", Toast.LENGTH_SHORT).show();

                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });


        ib_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        });

    }

    //    adding user activities to server for report generation
    private void addDataForReportGeneration() {

        if (isNetworkAvailable()) {

            final Realm realm = Realm.getInstance(getApplicationContext());


            final RealmResults<ReportData> results =
                    realm.where(ReportData.class).findAll();

            if (results.size() > 0) {

//                for (final ReportData reportData : results) {


                for (int i = 0; results.size() > i; i++) {

                    final int finalI = i;

                    final String category = results.get(i).getCategoty();
                    final String date = results.get(i).getDate();

                    RequestQueue mRequestQueue = Volley.newRequestQueue(MainActivity.this);
                    StringRequest req = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("ADD REPORT", response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            Log.i("ADD REPORT", error.toString());
                        }
                    }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("category", category + "");
                            params.put("date", date + "");

                            return params;
                        }
                    };

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mRequestQueue.add(req);

                }

                realm.beginTransaction();
                realm.clear(ReportData.class);
                realm.commitTransaction();

            }
        }
    }

    private void showData() {


//        loadDataFromDatabase();


        swipeRefreshLayout.setRefreshing(false);


        if (isNetworkAvailable()) {


            //swipeRefreshLayout.setRefreshing(true);


            // sending gcm id to server by generating gcm token
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);


            swipeRefreshLayout.setRefreshing(true);
            DataLoader2 mDataLoader2 = new DataLoader2(this, new DataLoader2.DataLoadedListener() {
                @Override
                public void onSuccess() {
                    Log.i("Dataloader2", "DATA LOADED");
                    swipeRefreshLayout.setRefreshing(false);
                    loadDataFromDatabase();
                }

                @Override
                public void onError() {
                    Log.i("Dataloader2", "Error in loading data");
                }
            });

        } else {

            Toast.makeText(getApplicationContext(), "Please connect to the Internet!", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

// loads data from database
    private boolean loadDataFromDatabase() {

        items1.clear();
        Realm realm = Realm.getInstance(getApplicationContext());
        final RealmResults<RssItems> results =
                realm.where(RssItems.class)
                        .findAllSorted("date", RealmResults.SORT_ORDER_DESCENDING);
        for (int i = 0; i < results.size(); i++) {
            items1.add(results.get(i));
        }


        newsListAdapter = new NewsListAdapter(MainActivity.this, items1, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = recyclerView.getChildAdapterPosition(v);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                String itemId = results.get(position).getId();
                intent.putExtra("itemId", itemId);
                startActivity(intent);

            }
        });
// sets the adapter
        mLayoutManager = new LinearLayoutManager(MainActivity.this);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(newsListAdapter);
        recyclerView.addItemDecoration(new ItemDevider(MainActivity.this));


        if (items1.size() > 0) {
            //  if the size of item is greater than 0 then returns false
            return false;
        } else {
            // if the size of the item is less than 0 returns true
            return true;
        }
    }

}


